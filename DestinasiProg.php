<?php

	

?>

<html>

	<head>

		<title> Menu Destinasi </title>

		<style>

			* {

				margin : 0;
				padding : 0;
			}

			body {

				font-family : Times New Roman ;

			}

			@font-face {

				font-family: "Times New Roman";

				src: url('jonathan_ball_-_acre-medium-webfont.woff2') format('woff2'),

				url('jonathan_ball_-_acre-medium-webfont.woff') format('woff');

				font-weight: normal;

				font-style: normal;

			}

			.ContainerHeader {
				
				width : 100%;
				height : 85px;
				border-bottom: 5px solid #979595;
				box-shadow: 0px 2px 4px #979595;

			}

			.HeaderKiri {

				float : left;
				margin-top : 10px;
				margin-left : 30px;

			}

			.LogoTourDera {

				width : 230px;
				height: 85px;


			}

			.HeaderKanan {

				float : right;
				margin-top : 30px;
				margin-right : 30px;

			}

			.ButtonLogin {

				background : none;
				border : none;
				margin : 0;
				padding : 0;
				cursor : pointer;
				font-size : 18px;

			}

			.ButtonRegister {

				background : none;
				border : none;
				margin : 0;
				padding : 0;
				cursor : pointer;
				font-size : 18px;
				margin-left : 15px;

			}

			.ContainerBackgrounHelloCoders {

				width : 70%;
				height : 55%;
				margin-top : 60px;
				background-image : url("ikon-kota-beriman.jpg");
				border-radius : 10px;
				box-shadow: 5px 5px 4px #979595;
				text-align : left;


			}

			.ContainerHello {

				padding-top : 45px;
				margin-left : 30px;

			}

			.LabelHello {

				font-size : 55px;

			}


			.LabelHelloWelcome {

				margin-top : 20px;

			}

			.ContainerWelcome {

				margin-top : 6px;

			}

			.ContainerItem {

				width : 70%;
				height : 90%;
				margin-top : 30px;

			}

			.ItemKiri {

				float : left;
				width : 32%;
				height : 65%;
				border-radius : 5px;
				box-shadow: 0px 5px 4px #979595;
				background-color :  white;

			}

			.TamanIcon {

				margin-top : 1px;
				width : 100%;
				height: 45%;

			}

			.ContainerTamanLabel {

				width : 100%;
				height :60%;
				margin-top : 20px;
				text-align : left;
				border-radius : 5px;
				box-shadow: 0px 5px 4px #979595;
				background-color : white;

		
			}

			.ItemTengah {

				display : inline-block;
				width : 32%;
				height : 60%;
				border-radius : 5px;
				box-shadow: 0px 5px 4px #979595;
				background-color :  white;

			}

			.PantaiIcon {

				margin-top : 1px;
				width : 100%;
				height: 50%;

			}

			.ContainerPantaiLabel {

				width : 100%;
				height : 65%;
				margin-top : 20px;
				text-align : left;
				border-radius : 5px;
				box-shadow: 0px 5px 4px #979595;
				background-color : white;

			}

			.ButtonJavaWeb:hover {

				background-color: white;
				border: 2px solid black;
				color: black;
			}

			.ItemKanan {

				float : right;
				width : 32%;
				height : 65%;
				border-radius : 5px;
				box-shadow: 0px 5px 4px #979595;
				background-color :  white;

			}

			.CarribeanIcon {

				margin-top : 1px;
				width : 100%;
				height: 45%;

			}

			.ContainerCarribeanLabel {

				width : 100%;
				height : 60%;
				margin-top : 20px;
				text-align : left;
				border-radius : 5px;
				box-shadow: 0px 5px 4px #979595;
				background-color : white;

			}

			

			.ButtonPythonWeb:hover {

				background-color: white;
				border: 2px solid black;
				color: black;
			}

			.StickyDiv {

				height : 5%;
				background-color : #ABADAE;
				text-align : center;
 				position: sticky;
 				position: -webkit-sticky;
 				top: 0;

			}

			.ModalLogin {

				display: none;
				position: fixed;
				z-index: 1;
				left: 0;
				top: 0;
				width: 100%;
				height: 100%;
				overflow: auto;
				background-color: rgb(0,0,0);
				background-color: rgba(0,0,0,0.4);
				padding-top: 60px;

			}	

			.ModalLogin .modal-content {

				background-color: white;
				margin: 10% auto 15% auto;
				border: 1px solid #888;
				width: 40%;
				height : 310px;

			}

			.TutupModal {

				color: #000;
				font-size: 35px;
				font-weight: bold;

			}

			.TutupModal:hover, .TutupModal:focus {

				color: red;
				cursor: pointer;

			}

			.animate {

				-webkit-animation: animatezoom 0.6s;
				animation: animatezoom 0.6s

			}

			@-webkit-keyframes animatezoom {

				from {-webkit-transform: scale(0)} 
				to {-webkit-transform: scale(1)}

			}
  
			@keyframes animatezoom {

				from {transform: scale(0)} 
				to {transform: scale(1)}

			}

			.ContainerHeaderModalLogin {

				width : 100%;
				height : 65px;
				background-color : #ECEEEF;
				border-bottom: 5px solid #979595;
				box-shadow: 0px 2px 4px #979595;

			}

			.ContainerFormModalLogin {

				width : 100%;
				height : 140px;
				margin-top : 10px;
				border-bottom: 5px solid #979595;
				border-top: 2px solid #979595;
				box-shadow: 0px 2px 4px #979595;

			}

			.ContainerBottomModalLogin {

				width : 100%;
				height : 75px;
				margin-top : 10px;
				background-color : #ECEEEF;
				border-bottom: 5px solid #979595;
				border-top: 2px solid #979595;
				box-shadow: 0px 2px 4px #979595;

			}

			input[type=text], input[type=password] {

				width : 40%;
				height : 30px;
				margin-top : 7px;
				box-shadow : 3px 3px 6px #00000029;
				border : 1px solid #00000029;
				border-radius : 6px;
				padding-left : 10px;
				font-family : acremedium;
				font-size : 15px;

			}

			::-webkit-input-placeholder {

				color: #00000029;
				font-family : acremedium;
				font-size : 15px;

			}

			.ButtonCloseLoginModal {

				width : 100px;
				height : 40px;
				margin-top : 13px;
				font-size : 16px;
				color : white;
				cursor : pointer;
				border-radius: 3px;
				border: 2px solid grey;
				transition-duration: 0.4s;
				background-color : grey;
				box-shadow: -2px 5px 4px #979595;

			}

			.ButtonCloseLoginModal:hover {

				background-color: white;
				border: 2px solid black;
				color: black;
			}

			.ButtonLoginModal {

				width : 100px;
				height : 40px;
				margin-top : 13px;
				font-size : 16px;
				color : white;
				cursor : pointer;
				border-radius: 3px;
				border: 2px solid #1FAEF6;
				transition-duration: 0.4s;
				background-color : #1FAEF6;
				box-shadow: -2px 5px 4px #979595;

			}

			.ButtonLoginModal:hover {

				background-color: white;
				border: 2px solid black;
				color: black;
			}

			.ModalRegister {

				display: none;
				position: fixed;
				z-index: 1;
				left: 0;
				top: 0;
				width: 100%;
				height: 100%;
				overflow: auto;
				background-color: rgb(0,0,0);
				background-color: rgba(0,0,0,0.4);
				padding-top: 60px;

			}	

			.ModalRegister .modal-content {

				background-color: white;
				margin: 10% auto 15% auto;
				border: 1px solid #888;
				width: 40%;
				height : 450px;

			}

			.TutupRegister {

				color: #000;
				font-size: 35px;
				font-weight: bold;

			}

			.TutupRegister:hover, .TutupModal:focus {

				color: red;
				cursor: pointer;

			}

			.ContainerHeaderModalRegister {

				width : 100%;
				height : 65px;
				background-color : #ECEEEF;
				border-bottom: 5px solid #979595;
				box-shadow: 0px 2px 4px #979595;

			}

			.ContainerFormModalRegister {

				width : 100%;
				height : 280px;
				margin-top : 10px;
				border-bottom: 5px solid #979595;
				border-top: 2px solid #979595;
				box-shadow: 0px 2px 4px #979595;

			}

			.ContainerBottomModalRegister {

				width : 100%;
				height : 75px;
				margin-top : 10px;
				background-color : #ECEEEF;
				border-bottom: 5px solid #979595;
				border-top: 2px solid #979595;
				box-shadow: 0px 2px 4px #979595;

			}

			.ButtonCloseRegisterModal {

				width : 100px;
				height : 40px;
				margin-top : 13px;
				font-size : 16px;
				color : white;
				cursor : pointer;
				border-radius: 3px;
				border: 2px solid grey;
				transition-duration: 0.4s;
				background-color : grey;
				box-shadow: -2px 5px 4px #979595;

			}

			.ButtonCloseRegisterModal:hover {

				background-color: white;
				border: 2px solid black;
				color: black;
			}

			.ButtonRegisterModal {

				width : 100px;
				height : 40px;
				margin-top : 13px;
				font-size : 16px;
				color : white;
				cursor : pointer;
				border-radius: 3px;
				border: 2px solid #1FAEF6;
				transition-duration: 0.4s;
				background-color : #1FAEF6;
				box-shadow: -2px 5px 4px #979595;

			}

			.ButtonRegisterModal:hover {

				background-color: white;
				border: 2px solid black;
				color: black;
			}

		</style>

	</head>

	<body>

		<div class = "ContainerHeader">

			<div class = "HeaderKiri">

				<img src = "Tourdera.PNG" class = "LogoTourDera">

			</div>

			<div class = "HeaderKanan">



					<button class = "ButtonLogin" onClick = "document.getElementById('DivModalLogin').style.display = 'block'"> Login </button> <br>

					<button CLASS = "ButtonRegister" onClick = "document.getElementById('DivModalRegister').style.display = 'block'"> Sign Up </button>


			</div>

		</div>

		<center>

			<div class = "ContainerBackgrounHelloCoders">

				<div class = "ContainerHello">

					<label class = "LabelHello"> WELCOME TO BALIKPAPAN </label>

						<br>

					<div class = "ContainerWelcome">

						<label> CARI PAKET LIBURAN DI BALIKPAPAN </label>

					</div>

				</div>

			</div>

			<div class = "ContainerItem">
				
				<div class = "ItemKiri">

					<img src = "Tempat-Wisata-Alam-Hutan-Bakau-Margomulyo-Balikpapan.jpg" class = "TamanIcon">

					<div class = "ContainerTamanLabel">

						<div style = "margin-left : 10px;">

							<label style = "font-size : 20px; color:  #465927;"> <b> Hutan Bakau Margomulyo <b> </label>

						</div>

							<br>
							<br>

						<div style = "margin-top : -10px; margin-left : 10px; margin-right : 10px;  text-align: justify; text-justify: inter-word;">

							<label style="color: #637c36;"> Lokasi ini dekat dengan pusat kota. Dilokasi ini anda akan menemukan jembatan kayu sepanjang 800 meter yang berfungsi sebagai penunjang sarana bagi anda yang ingin menikmati susasana alam dihutan tersebut. </label>

						</div>

					</div>


				</div>

				<div class = "ItemKanan">

					<img src = "Morgans-Island-Photos-for-RELEASE-746398_800x450.jpg" class = "CarribeanIcon">

					<div class = "ContainerCarribeanLabel ">

						<div style = "margin-left : 20px;">

							<label style = "font-size : 16px; color:  #800000;"> <b> Carribean Island Waterpark <b> </label>

						</div>

						

							<br>

						<div style = "margin-top : -10px; margin-left : 10px; margin-right : 10px;  text-align: justify; text-justify: inter-word;">

							<label style="color: #b30000;"> Inilah taman wisata air terbesar di Indonesia Bagian Timur, Caribbean Island Waterpark. Caribbean Island Waterpark menawarkan berbagai wahana air yang sungguh menarik. Caribbean Island Waterpark ini menjadi destinasi favorit wisata keluarga di Balikpapan. </label>

						</div>

					</div>


				</div>

				<div class = "ItemTengah">

					<img src = "pintu-masuk-pantai-lamaru-kalimantan-timur_800x431.jpg" class = "PantaiIcon">

					<div class = "ContainerPantaiLabel">

						<div style = "margin-left : 10px;">

							<label style = "font-size : 20px; color: #997300;"> <b> Pantai Lamaru <b> </label>

						</div>

							<br>

						<div style = "margin-top : -10px; margin-left : 10px; margin-right : 10px;  text-align: justify; text-justify: inter-word;">

							<label style="color: #cc9900;"> Pantai Lamaru adalah objek wisata pantai di Balikpapan yang menawarkan suasana tenang bagi Anda yang menginginkan tempat bersantai yang tidak ramai. Pepohonan cemara di areal pantai ini memberikan Anda ketenangan yang begitu nyaman dengan suguhan panorama laut yang indah.  </label>

						</div>

					</div>

		

				</div>

			</div>

		</center>

		<div class = "StickyDiv">

			<div style = "padding-top : 10px;">

				<label> Go To  </label>

			</div>

		</div>

		<div id = "DivModalLogin" class = "ModalLogin">

			<form class = "modal-content animate" action = "" method = "POST">

				<div class = "ContainerHeaderModalLogin">

					<div style = "float : left; margin-top : 9px; margin-left : 20px;">

						<label style = "font-size : 30px;"> Login </label>

					</div>

					<div style = "float : right; margin-top : 5px; margin-right : 20px;">

						<span onClick = "document.getElementById('DivModalLogin').style.display = 'none'" class = "TutupModal" title = "Tutup Form Login"> &times; </span>

					</div>

				</div>

				<div class = "ContainerFormModalLogin">

					<div style = "margin-top : 6px; margin-left : 10px;">

						<label for = "email"> Email Address </label>

							<br>

						<input type = "text" name = "email" placeholder = "Enter email" required>

							<br>

						<label for = "password"> Password </label>

							<br>

						<input type = "password" name = "password" placeholder = "Password" required>

							<br>

					</div>

				</div>

				<div class = "ContainerBottomModalLogin">

					<div style = "width : 20%; float : right;">

						<button type = "submit" class = "ButtonLoginModal"> Login </button>

					</div>

					<div style = "width : 20%; float : right;">

						<button type = "button" class = "ButtonCloseLoginModal" onclick = "document.getElementById('DivModalLogin').style.display = 'none'"> Close </button>

					</div>

				</div>

			</form>

		</div>

		<div id = "DivModalRegister" class = "ModalRegister">

			<form class = "modal-content animate" action = "Home-Registration.php" method = "POST">

				<div class = "ContainerHeaderModalRegister">

					<div style = "float : left; margin-top : 9px; margin-left : 20px;">

						<label style = "font-size : 30px;"> Register </label>

					</div>

					<div style = "float : right; margin-top : 5px; margin-right : 20px;">

						<span onClick = "document.getElementById('DivModalRegister').style.display = 'none'" class = "TutupModal" title = "Tutup Form Register"> &times; </span>

					</div>

				</div>

				<div class = "ContainerFormModalRegister">

					<div style = "margin-top : 6px; margin-left : 10px;">

						<label for = "Email"> Email Address </label>

							<br>

						<input type = "text" name = "Email" placeholder = "Enter email" required>

							<br>

						<label for = "Username"> Enter Username </label>

							<br>

						<input type = "text" name = "Username" placeholder = "Enter username" required>

							<br>

						<label for = "password"> Password </label>

							<br>

						<input type = "password" id = "password" name = "Password" placeholder = "Password" required>

							<br>

						<label for = "Repassword"> Confirm Password </label>

							<br>

						<input type = "password" id = "confirmpassword" name = "Repassword" placeholder = "Confirm Password" required>

							<br>

					</div>

				</div>

				<div class = "ContainerBottomModalRegister">

					<div style = "width : 20%; float : right;">

						<button type = "submit" class = "ButtonRegisterModal"> Register </button>

					</div>

					<div style = "width : 20%; float : right;">

						<button type = "button" class = "ButtonCloseRegisterModal" onclick = "document.getElementById('DivModalRegister').style.display = 'none'"> Close </button>

					</div>

				</div>

			</form>

		</div>

		<script>

			var password = document.getElementById("password") , confirm_password = document.getElementById("confirmpassword");

			function validatePassword(){

				if(password.value != confirm_password.value) {

					confirm_password.setCustomValidity("Passwords Tidak Sama!!!");

				} else {

					confirm_password.setCustomValidity('');

				}

			}

			password.onchange = validatePassword;
			confirm_password.onkeyup = validatePassword;

		</script>

	</body>

</html>
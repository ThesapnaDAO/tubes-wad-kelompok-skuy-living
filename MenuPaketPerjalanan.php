<!DOCTYPE html>
<html>
<head>
	<title>Paket Perjalanan</title>
	<meta charset="utf-8">
	<style>
		body {
			background-color: #A9A9A9;
			font-family: sans-serif;
		}
		.navbar {
			background-color: white;
			width: 100%;
			padding: 23px;
		}
		.logo {
			width: 20%;
			margin-top: -50px;
			margin-bottom: -45px;
			float: left;
		}
		.search {
			width: 100%
			padding: 50px;
			border-radius: 12px;
			height: 40px;
			display: inline-block;
			border: 1px solid #808080;
			box-sizing: border-box;
			margin-left: 150px;
			margin-top: -50px;
			font-size: 10px;
			margin-right: 60px;
		}
		.nav {
			font-size: 16px;
			color: #C0C0C0;
			padding: 35px;
			margin-bottom: 20px;

		}
		.indo {
			width: 2%;
			margin-right: 20px;
			margin-left: 20px;
			margin-top: 20px;
		}
		.reg {
			float: right;
			color: #708090;
			font-size: 18px;
			padding: 15px;
			margin-right: 60px;
		}
		.raup {
			background-image: url("raup.png");
			width: 100%;
			height: 600px;
			background-size: 1500px 650px;
		}
		.kmp {
			background-color: white;
			float: right;
			border: 1px solid
			background-color: white;
			border-radius: 12px;
			padding: 5px 10px;
			margin-right: 10px;
		}
		.rate {
			margin-left: 10px;
		}
		.vis {
			color: white;
			margin-left: 100px;
			font-size: 25px;
		}
		.detail {
			background-color: white;
		}
		.gmb {
			border-radius: 70px;
			width: 271px;
			height: 160px;
			margin: 4px;
			border: 1px solid;
			border-color: black;
		}
		.paket {
			background-color: #1E99FF;
			color: white;
			text-align: center;
			padding: 8px 150px;
			width: 50%;
			border-radius: 20px;
			font-size: 20px;
		}
		.pkt {
			margin-top: 10px;
			background-color: white;
			padding: 1px;
		}
		.gmb2 {
			width: 350px;
			height: 250px;
			padding: 10px;
		}
		.container {
		  	position: relative;
		  	text-align: center;
		  	margin-bottom: 25px;
		}
		.left1 {
		  	position: absolute;
		 	top: 210px;
		  	left: 162px;
		  	background-color: #FFD700;
		  	color: white;
		  	padding: 5px 10px;
		  	font-size: 25px;
		  	opacity: 80%;
		}
		.left2 {
			position: absolute;
		 	top: 210px;
		  	left: 536px;
		  	background-color: #FFD700;
		  	color: white;
		  	padding: 5px 10px;
		  	font-size: 25px;
		  	opacity: 80%;
		}
		.left3 {
			position: absolute;
		 	top: 210px;
		  	left: 910px;
		  	background-color: #FFD700;
		  	color: white;
		  	padding: 5px 10px;
		  	font-size: 25px;
		  	opacity: 80%;
		}
		.see {
			font-size: 30px;
			color: #808080;
		}
		.sejarah {
			background-color: white;
			margin-top: 20px;
			width: 100%;
		}
		h1{
			margin: 30px;
			margin-top: 20px;
			padding: 5px;
			color: #808080;
		}
		.sej {
			margin-left: 820px;
			margin-top: -120px;
			width: 600px;
			height: 400px;
		}
		.explore {
			color: white;
			font-size: 15px;
			padding: 8px 50px;
			background-color: #00BFFF;
			border-radius: 6px;
			margin-left: 100px;
			margin-bottom: 100px;
		}
		.review {
			background-color: white;
			width: 60%;
			float: left;
		}
	</style>
</head>
<body>
	<br>
	<img class="logo" src="tourdera.png">
	<div  class="navbar">
	<input class="search" type="text" placeholder="Cari Destinasi atau Aktvitas" size="50">
		<a href="#" class="nav">Trips</a>
		<img class="indo" src="indonesia.png">
		<a href="#" class="nav">Blog</a>
		<a href="#" class="nav">Information</a>
		<div class="reg">
			<a href="#" class="nav">LOG IN</a><br>
			<a href="#" class="nav">SIGN UP</a>
		</div>
	</div>
	<br>
	<div class="raup">
		<p class="kmp">KAMPUNG CAI RANCA UPAS</p>
		<br><br><br><br><br><br><br><br><br><br><br><br><br><br>
		<br><br><br><br><br><br><br><br><br><br><br><br><br>
		<img class="rate" src="rate.png"><br>
		<p class="vis">126 Visited</p>
	</div>
	<div class="detail">
		<img class="gmb" src="raup1.png">
		<img class="gmb" src="raup2.png">
		<img class="gmb" src="raup3.png">
		<img class="gmb" src="raup4.png">
		<img class="gmb" src="raup5.png">
	</div>
	<div class="pkt">
		<center>
		<p class="paket">PAKET DESTINASI WISATA KAMPUNG CAI RANCA UPAS</p>
		</center> 
		<div class= "container">
			<div class="left1">PAKET DESA</div>
			<div class="left2">PAKET TRAVELER</div>
			<div class="left3">PAKET ONE DAY TRIP</div>
			<img class="gmb2" src="desa.jpg">
			<img class="gmb2" src="traveler.png">
			<img class="gmb2" src="oneday.png">
		</div>
	</div>
	<div style="background-color: #FFFAF0;">
		<center>
		<p class="see"><u>SEE VILAGE ON SKY</u></p>
		<video controls style="width: 50%">
		<source src="rancaupas.mp4" type="video/mp4">
		</video>
		</center>
		<br>
	</div>
	<div class="sejarah">
		<br>
		<h1>Yuk Mengenal...</h1>
		<h1>Kampung Cai Ranca Upas</h1>
		<img src="sejarah.png" class="sej">
		<h1 style="margin-left: 880px;">Sejarah</h1>
		<button class="explore" type="button">Explore More</button>
	</div>
	<div class="cust">
		<div class="review">
			<br>
			<p style="margin-left: 100px;">Review</p>
		</div>
		<div class="contact">
			<p>TOUDERA CUSTOMER CARE</p>
		</div>
	</div>
</body>
</html>
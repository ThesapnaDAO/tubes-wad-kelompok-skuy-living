<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="">
        <meta name="description" content="" />
        <meta name="author" content="">
        <title></title>
        <link rel="Shortcut Icon" href="/assets/images/logo/" type="image/x-icon" />
        
        <link href="css/bootstrap.css" rel="stylesheet">
        
        <link href="css/style_admin.css" rel="stylesheet">
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

<div id="wrapper">
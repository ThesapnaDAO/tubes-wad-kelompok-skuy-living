<html>
<head>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<title>Hello, Please Login!</title>
</head>
<body>
	<nav class="atas">
		<a href="welcome.php"><img src="tourdera2.png" width="100px" class="logo"></a>
		<input type="text" name="cari" class="cari" placeholder="Cari Destinasi atau Aktivitas">
		<div class="divnav">	
			<a href="">Trips</a>
			<img src="indonesia.png" width="20px">
			<a href="">Blog</a>
			<a href="">Information</a>
			<a href="">LOG IN</a>
		</div>
	</nav>
	<div class="tengah">
		<img class="img" src="borobudur.jpg">
		<form class="form_login" action="aksi_login.php" method="POST" onSubmit="return validasi()">
			<h2 class="h2_hello">Hello, Please Login!</h2>
			<h4>Dont have an account? <a class="a_sign" href="sign.php"><b>Sign</b></a></h4>
			<table align="center">
				<tr>
					<td>
						<label class="lbl">Email address</label><br>
						<input type="text" name="email" id="email"> 
					</td>
				</tr>
				<tr>
					<td>
						<label class="lbl">Password</label><br>
						<input type="password" name="password" id="password">
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<button class="btn_facebook"><i class="fa fa-facebook"></i>Join via facebook</button>
					</td>
				</tr>
				<tr>
					<td>
						<input type="submit" name="login" class="btn_login" value="LOGIN">
					</td>
				</tr>
			</table>
			<h6>By joining, you agree to the <b>Terms</b> and <b>Privacy Policy</b></h6>
		</form>
	</div>
	<div class="bawah">
		<div class="connect">
			<h1>Connect With Us</h1>
			<a href=""><i class="fa fa-twitter"></i></a>
			<a href=""><i class="fa fa-instagram"></i></a>
			<a href=""><i class="fa fa-facebook"></i></a>
			<a href=""><i class="fa fa-google-plus"></i></a>
		</div>
		<div class="our_company">
			<a href="">Our Company</a><br>
			<a href="">Friends</a><br>
			<a href="">Join Mitra Kami</a><br>
			<a href="">Cara Kerja Tourdera</a><br>
			<a href="">Kebijakan</a><br>
			<a href="">Syarat dan Ketentuan</a>
		</div>
		<div class="form_contact">
			<table class="tbl_contact">
			<h1>Contact Us</h1>
				<tr>
					<td>
						<label>First Name</label><br>
						<input type="text" name="Firstname" placeholder="First Name">
					</td>
					<td>
						<label>Last Name</label><br>
						<input type="text" name="Lastname"z placeholder="Last Name">
					</td>
				</tr>
				<tr>
					<td>
						<label>Email</label><br>
						<input type="text" name="email" placeholder="Email">
					</td>
					<td>
						<label>Phone</label><br>
						<input type="number" name="phone" placeholder="Phone">
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<label>Message</label><br>
						<textarea placeholder="Your message"></textarea>
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<button class="send" name="send">Send</button>
					</td>
				</tr>
			</table>
		</div>
		<hr>
		<div>
			<p class="copyright">&copy; 2019 Tourdera.com</p>
			<p class="cookies">Privacy Policy Terms of Use Cookies Policy</p>
		</div>
	</div>

<!--sumber : https://www.malasngoding.com/membuat-form-validasi-dengan-javascript/ -->
<script type="text/javascript">
	function validasi(){
		var email = document.getElementById("email").value;
		var password = document.getElementById("password").value;

		if(email != "" || password  != ""){
			return true;
		}
		else{
			alert("Email dan Password harus diisi!");
			return false;
		}
	}
</script>
<style type="text/css">
	body{
		color: grey;
		font-family: sans-serif;
	}
	.form_login{
		margin-top: 50px;
		width: 40%;
		float: right;
	}
	table{
		width: 70%;
	}
	.h2_hello{
		color: black;
		text-align: center;
	}
	h4, h6{
		font-weight: normal;
		text-align: center;
	}		
	td{
		height: 50px;
	}
	.lbl{
		font-size: 12px;
	
	}
	.atas{
		position: fixed;
		top: 0;
		right: 0;
		left: 0;
		background-color: #f2f2f2;
	}
	.cari{
		background-color: white;
		height: 20px;
		padding-left: 4px;
		margin: 0px 0px 0px 300px;		 
		border-radius: 10px;
		width: 170px;
	}
	.atas a{
		text-align: center;
		text-decoration: none;
		padding: 14px 16px;
		color: grey;
		max-width: 50%;
	}
	input{
		width: 100%;
		height: 25px;
		background-color: #f2f2f2;
		border: 1px solid lightgrey;
	}
	.btn_facebook{
		width: 100%;
		background-color: #3b5998;
		color: white;
		border: none;
		border-radius: 2px;
		height: 30px;
		margin-top: 30px;
	}
	.btn_facebook .fa{
		width: 10px;
		 background-color: transparent;
		 border-radius: 50%;
	}
	.btn_login{
		background-color: #33cc33;
		color: white;
		border: none;
		border-radius: 2px;
		height: 30px;		
	}
	.a_sign{
		font-weight: bold;
		text-decoration: none;
		color: grey;
	}
	.tengah{
		width: 100%;
	}
	.img{
		width: 700px;
		margin-left: -8%;
		margin-top: -5%;
		border-radius: 50%;
	}
	i{
		 width: 15px;
		 background-color: white;
		 border-radius: 50%;
		 padding:8px;
		 margin-left: 10px;
	}
	.bawah{
		margin-top: 20px;
		background-color: #f2f2f2;
		width: 100%;
	}
	.connect{
		background-color: #f2f2f2;
		width: 30%;
		float: left;
		margin-bottom: 100px;
	}
	.form_contact{
		width: 30%;
		overflow: auto;
		background-color: #f2f2f2;
	}
	.divnav{
		margin: 10px 20px 10px 20px;
		float: right;
		max-width: 50%;
	}
	.send{
		width: 100%;
		height: 25px;
		color: white;
		background-color: #0080fe;
		border: none;
		border-radius: 5px;
	}
	.tbl_contact td input{
		background-color: white;
	}
	.tbl_contact{
		width: 100%;
	}
	.tbl_contact td{
		width: 50%;
	}
	.copyright{
		max-width: 50%;
	}
	.cookies{
		max-width: 50%;
		overflow: auto;
		text-align: right;
	}
	.our_company a{
		text-align: center;
		text-decoration: none;
		padding: 14px 16px;
		color: grey;
		max-width: 50%;
		font-size: 12px;
	}
	.our_company{
		width: 20%;
		overflow: auto;
	}
	.connect a{
		color: inherit;
	}

	.logo{
		margin: 7px auto auto 20px;
	}
</style>
</body>
</html>

<?php  

?>

<html>
<head>
	<title>Menu Admin</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<style>
		.input {
			margin-left: 200px;
			margin-top: 50px;
			padding: 10px;
		}
		input {
			margin-left: 30px;
			margin-bottom: 10px;
			padding: 10px 300px;
		}
		button {
			padding: 5px 50px;
			border-radius: 6px;
			margin: 30px;
			background-color: darkgrey;
			border-color: black;
		}
	</style>
</head>
<body>
	<nav class="navbar navbar-inverse">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <a class="navbar-brand" href="#">Tourdera Admin</a>
	    </div>
	    <ul class="nav navbar-nav">
	      <li class="active"><a href="#">Menu Tambah Desa</a></li>
	      <li><a href="#">Menu Transaksi</a></li>
	      <li><a href="#">Menu Konfirmasi Tiket</a></li>
	    </ul>
	    <ul class="nav navbar-nav navbar-right">
	      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Other<span class="caret"></span></a>
	        <ul class="dropdown-menu">
	          <li><a href="#">Home</a></li>
	          <li><a href="#">About</a></li>
	          <li><a href="#">Logout</a></li>
	        </ul>
	      </li>
	    </ul>
	  </div>
	</nav>
	<div class="container">
	  <center><h2>MENU TAMBAH DESA</h2>
	  <br><br><br>
		<img src="noimages.png" alt="">
	</center>
    </div>
    <hr>
   	<div class="input">
   	<form method="POST" action="inputkota.php" enctype="multipart/form-data">
		<table class="table-responsive" >
			<tr>
				<td>Nama Kota</td>
				<td><input type="text" name="kota"></td>
			</tr>
			<tr>
				<td>Nama Wisata</td>
				<td><input type="text" name="namawisata"></td>
			</tr>
			<tr>
				<td>Gambar Wisata</td>
				<td><input type="file" accept="image/*" name="gambarwisata"></td>
			</tr>
			<tr>
				<td>Harga Paket</td>
				<td><input type="text" name="paketwisata"></td>
			</tr>
			<tr>
				<td>Deskripsi</td>
				<td><input type="text" name="desc"></td>
			</tr>
		</table>
	</form>
	</div>
	<div>
		<center>
        <button><a href="confirmdesa.php">Submit Desa<i class="fa fa-arrow-circle-right"></i></a></button>
    	</center>
    </div>
</body>
</html>
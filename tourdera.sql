-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 18, 2019 at 06:21 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tourdera`
--

-- --------------------------------------------------------

--
-- Table structure for table `td_admin`
--

CREATE TABLE `td_admin` (
  `id_admin` int(15) NOT NULL,
  `nama_adm` text NOT NULL,
  `jenkelamin` text NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `td_bank`
--

CREATE TABLE `td_bank` (
  `id_bank` int(15) NOT NULL,
  `nama_bank` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `td_booking`
--

CREATE TABLE `td_booking` (
  `id_booking` int(20) NOT NULL,
  `id_user` int(15) NOT NULL,
  `id_admin` int(15) NOT NULL,
  `jmlh_tiket` int(11) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `td_user`
--

CREATE TABLE `td_user` (
  `id_user` int(30) NOT NULL,
  `nama_user` text NOT NULL,
  `username` varchar(30) DEFAULT NULL,
  `passwd` varchar(20) DEFAULT NULL,
  `alamat` varchar(50) DEFAULT NULL,
  `no_tlp` varchar(15) DEFAULT NULL,
  `gender` varchar(15) DEFAULT NULL,
  `tgl_lahir` varchar(20) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `image` longblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `td_user`
--

INSERT INTO `td_user` (`id_user`, `nama_user`, `username`, `passwd`, `alamat`, `no_tlp`, `gender`, `tgl_lahir`, `email`, `image`) VALUES
(1, 'Risky Novendri', 'novendri', 'akuaku', 'Jln.Mangga 2 no.37', '082280366801', 'Laki-Laki', '01 November 1998', 'riskynovendri@gmail.com', '');

-- --------------------------------------------------------

--
-- Table structure for table `td_wisata`
--

CREATE TABLE `td_wisata` (
  `id_kota` int(100) UNSIGNED NOT NULL,
  `nama_kota` varchar(200) NOT NULL,
  `nama_wisata` varchar(100) DEFAULT NULL,
  `gambar_kota` varchar(100) DEFAULT NULL,
  `harga_paket` varchar(100) DEFAULT NULL,
  `deskripsi` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `td_admin`
--
ALTER TABLE `td_admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `td_bank`
--
ALTER TABLE `td_bank`
  ADD PRIMARY KEY (`id_bank`);

--
-- Indexes for table `td_booking`
--
ALTER TABLE `td_booking`
  ADD PRIMARY KEY (`id_booking`);

--
-- Indexes for table `td_user`
--
ALTER TABLE `td_user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `td_wisata`
--
ALTER TABLE `td_wisata`
  ADD PRIMARY KEY (`id_kota`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `td_admin`
--
ALTER TABLE `td_admin`
  MODIFY `id_admin` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `td_bank`
--
ALTER TABLE `td_bank`
  MODIFY `id_bank` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `td_booking`
--
ALTER TABLE `td_booking`
  MODIFY `id_booking` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `td_user`
--
ALTER TABLE `td_user`
  MODIFY `id_user` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `td_wisata`
--
ALTER TABLE `td_wisata`
  MODIFY `id_kota` int(100) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
